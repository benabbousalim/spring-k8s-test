#!/bin/sh
webserv=$1

COUNT_BEFORE=$(curl --header "Content-Type: application/json" http://$webserv/person | jq length)

if [[ $COUNT_BEFORE == 0 ]]
then
    echo "Count is zero"
else
    echo "Count is different than zero"
fi


PAYLOAD='{"name":"xyz"}'


RESULTAT=$(curl --header "Content-Type: application/json" \
  --request POST \
  --data $PAYLOAD \
  http://$webserv/person | jq '. | {name: .name}')

if [[ $RESULTAT == $PAYLOAD ]]
then
    echo "Object created"
else
    echo "Object not created"
fi

COUNT_AFTER=$(curl --header "Content-Type: application/json" http://$webserv/person | jq length)

if [[ $COUNT_AFTER == 1 ]]
then
    echo "Count is one"
else
    echo "Count is different than one"
fi
