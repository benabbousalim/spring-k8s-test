package ma.co.omnishore.restapi.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.co.omnishore.restapi.model.Person;
import ma.co.omnishore.restapi.repository.PersonRepository;

/**
 * PersonController
 */
@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonRepository repository;
    Logger log = LoggerFactory.getLogger(PersonController.class);


    @GetMapping
    List<Person> getAllPerson() {
        return this.repository.findAll();
    }

    @PostMapping
    Person createPerson(@RequestBody Person person){
        return this.repository.save(person);
    }
}
