package ma.co.omnishore.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.co.omnishore.restapi.model.Person;;

/**
 * PersonRepository
 */
public interface PersonRepository extends JpaRepository<Person, Long>{


}
