package ma.co.omnishore.restapi;

import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import ma.co.omnishore.restapi.model.Person;
import ma.co.omnishore.restapi.repository.PersonRepository;
import ma.co.omnishore.restapi.web.PersonController;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RestApiApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private PersonController controller;

	@Autowired
	private PersonRepository repository;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

	}

	@Test
	public void testGetPersons() throws Exception {
		Person emp1 = new Person("Test1");
		Person emp2 = new Person("Test2");
		repository.save(emp1);
		repository.save(emp2);

		mockMvc.perform(MockMvcRequestBuilders.get("/person").accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$", hasSize(2))).andReturn();
	}

}
